package com.cippapp.learningkotlin

import android.content.Context
import android.content.SharedPreferences

class PrefsHelper(context: Context) {
    private val PREFS_NAME= "counterSharedPreference"
    private val COUNTER_KEY = "counter"
    private var sharedPref: SharedPreferences = context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE)

    var counterValue: Int
        get() = sharedPref.getInt("counter",0)
        set(value) = sharedPref.edit().putInt("counter",value).apply()
}