package com.cippapp.learningkotlin.view_model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cippapp.learningkotlin.PrefsHelper


class CounterViewModel(app: Application) : AndroidViewModel(app)  {
    private var prefHelper: PrefsHelper = PrefsHelper(getApplication<Application>().applicationContext)
    var counter= MutableLiveData<Int>()
    init {
        (counter as? MutableLiveData)?.value = prefHelper.counterValue
    }

    fun increaseValue() {
        prefHelper.counterValue++
        setCounter()
    }

    fun decreaseValue() {
        prefHelper.counterValue--
        setCounter()
    }

    fun setCounter(){
        (counter as? MutableLiveData)?.value = prefHelper.counterValue
    }


}