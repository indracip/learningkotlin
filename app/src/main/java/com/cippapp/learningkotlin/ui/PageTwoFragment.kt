package com.cippapp.learningkotlin.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.cippapp.learningkotlin.PrefsHelper
import com.cippapp.learningkotlin.R
import com.cippapp.learningkotlin.databinding.FragmentPageTwoBinding
import com.cippapp.learningkotlin.view_model.CounterViewModel

class PageTwoFragment : Fragment() {
    lateinit var binding: FragmentPageTwoBinding
    lateinit var counterViewModel: CounterViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPageTwoBinding.inflate(inflater,container,false)
        binding.nextButton2.setOnClickListener {
            it.findNavController().navigate(R.id.myPageThreeFragment)
        }

        binding.prevButton2.setOnClickListener {
            it.findNavController().popBackStack()
        }

        counterViewModel = ViewModelProvider(this).get(CounterViewModel::class.java)
        binding.counterViewModel = counterViewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        counterViewModel.setCounter()
    }
}