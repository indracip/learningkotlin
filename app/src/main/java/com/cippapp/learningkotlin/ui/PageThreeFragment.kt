package com.cippapp.learningkotlin.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.cippapp.learningkotlin.PrefsHelper
import com.cippapp.learningkotlin.R
import com.cippapp.learningkotlin.databinding.FragmentPageThreeBinding
import com.cippapp.learningkotlin.view_model.CounterViewModel


class PageThreeFragment : Fragment() {

    lateinit var binding: FragmentPageThreeBinding
    lateinit var counterViewModel: CounterViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPageThreeBinding.inflate(inflater,container,false)

        binding.nextButton3.setOnClickListener {
            it.findNavController().navigate(R.id.myPageFourFragment)
        }

        binding.prevButton3.setOnClickListener {
            it.findNavController().popBackStack()
        }

        counterViewModel = ViewModelProvider(this).get(CounterViewModel::class.java)
        binding.counterViewModel = counterViewModel
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        counterViewModel.setCounter()
    }

}